package com.projectone;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.projectone.controller.ReimbursementController;
import com.projectone.controller.UserController;
import com.projectone.dao.DatabaseConnection;
import com.projectone.dao.ReimbursementDao;
import com.projectone.dao.UserDao;
import com.projectone.services.ReimbursementService;
import com.projectone.services.UserService;

import io.javalin.Javalin;

public class MainDriver {
	
	//Logger
	public final static Logger log = Logger.getLogger(MainDriver.class);

	
	private static UserController uControl = new UserController(new UserService(new UserDao(new DatabaseConnection())));
	private static ReimbursementController rControl = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new DatabaseConnection())));

	public static void main(String[] args){

		
		log.setLevel(Level.ALL);
		log.trace("trace logging edited");
		log.debug("debug loggin edited");
		
		Javalin app = Javalin.create(config ->{
			config.addStaticFiles("/frontend");
		}).start(2345);
		
		app.post("/HomePage/Login", uControl.postLogin);
		app.get("/Login/Session", uControl.getSession);
		app.post("/User/createReimbursement", rControl.createReimbursement);
		app.get("/User/Reimbursements", rControl.userReimbursements);
		app.get("/FinanceManager/Reimbursements", rControl.viewAllReimbursements);
		app.get("/Reimbursement/Approved", rControl.viewByApproved);
		app.get("/Reimbursement/Pending", rControl.viewByPending);
		app.get("/Reimbursement/Denied", rControl.viewByDenied);
		app.post("/Approved/:id", rControl.approveReimbursement);
		app.post("/Denied/:id", rControl.deniedReimbursement);
		

		app.exception(NullPointerException.class, (e, ctx) ->{
			log.debug("Null Exception Debug Logging Edited");
			ctx.status(404);
			ctx.redirect("/html/tryAgainPage.html");
		});
	}
	
	
}
