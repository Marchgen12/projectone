package com.projectone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.projectone.model.UserRoles;
import com.projectone.model.Users;

public class UserDao implements GenericDao<Users> {
	private DatabaseConnection dc;
	
	public UserDao() {
		dc = new DatabaseConnection();
	}
	
	public UserDao(DatabaseConnection dc) {
		super();
		this.dc = dc;
	}

	public Users selectById(int id) {
		Users user = new Users();
		try(Connection con = dc.getConnection()){
			String sql = "SELECT * FROM Users WHERE User_id = ?";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setInt(1, id);
			
			ResultSet rs = prepStat.executeQuery();
			
			while(rs.next()) {
				user = new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public Users selectByUsername(String username) {
		Users user = null;
		try(Connection con = dc.getConnection()){
			String sql = "SELECT * FROM Users WHERE Username= ?";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setString(1, username);
			
			ResultSet rs = prepStat.executeQuery();
			
			while(rs.next()) {
				user = new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public Users selectByEmail(String email) {
		Users user = null;
		try(Connection con = dc.getConnection()){
			String sql = "SELECT * FROM Users WHERE Email= ?";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setString(1, email);
			
			ResultSet rs = prepStat.executeQuery();
			
			while(rs.next()) {
				user = new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public UserRoles selectRole(String role) {
		UserRoles uRole = null;
		try(Connection con = dc.getConnection()){
			String sql = "SELECT * FROM User_Roles WHERE User_role = ?";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setString(1, role);
			
			ResultSet rs = prepStat.executeQuery();
			
			while(rs.next()) {
				uRole = new UserRoles(rs.getInt(1), rs.getString(2));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return uRole;
	}
}
