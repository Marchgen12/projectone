package com.projectone.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

	private String url = "jdbc:mariadb://database-1.cg7cn5qcgpss.us-east-2.rds.amazonaws.com:3306/project1ver2db";
	private String username = "project1ver2user";
	private String password = "mypassword";
	
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
}
