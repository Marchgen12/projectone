package com.projectone.dao;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.projectone.model.CustomizeReimbursementOutput;
import com.projectone.model.Reimbursement;
import com.projectone.model.ReimbursementStatus;
import com.projectone.model.ReimbursementType;

public class ReimbursementDao{
	private DatabaseConnection dc;
	
	public ReimbursementDao() {
		dc = new DatabaseConnection();
	} 

	public ReimbursementDao(DatabaseConnection dc) {
		super();
		this.dc = dc;
	}
	
	public List<CustomizeReimbursementOutput> selectByAuthorId(int authorId) {
		List<CustomizeReimbursementOutput> Reimbursements = new ArrayList<>();
		try(Connection con = dc.getConnection()){
			String sql = "SELECT r.*, rs.Status, rt.Type_name, u.First_name, u.Last_name FROM Reimbursement r "
					+ "JOIN Reimbursement_Status rs "
					+ "ON r.Reimb_author = ? AND r.Reimb_status_id = rs.Status_id "
					+ "JOIN Reimbursement_Type rt "
					+ "ON rt.Type_id = r.Reimb_type_id JOIN Users u ON u.User_id = r.Reimb_author";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setInt(1, authorId);
			
			ResultSet rs = prepStat.executeQuery();
			
			while(rs.next()) {
				Reimbursements.add(new CustomizeReimbursementOutput(rs.getString(13), rs.getString(14), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getString(11), rs.getString(12)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return Reimbursements;
	}
	
	public void createReimbursement(int amount, Timestamp submitted, String description, Blob receipt, int author, int status_id, int type_id) {
		try(Connection con = dc.getConnection()){
			String sql = "INSERT INTO Reimbursement(Reimb_amount, Reimb_submitted, Reimb_description, Reimb_recei"
					+ "pt, Reimb_author, Reimb_status_id, Reimb_type_id) "
					+ "VALUES(?,?,?,?,?,?,?)";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setInt(1,  amount);
			prepStat.setTimestamp(2, submitted);
			prepStat.setString(3, description);
			prepStat.setBlob(4, receipt);
			prepStat.setInt(5, author);
			prepStat.setInt(6, status_id);
			prepStat.setInt(7, type_id);
			
			prepStat.execute();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
//	public List<CustomizeReimbursementOutput> selectReimbursementByStatusAuthor(int Reimb_author, String status) {
//		List<CustomizeReimbursementOutput> statusReimbursements = new ArrayList<>();
//		try(Connection con = dc.getConnection()){
//			String sql = "SELECT r.*, rs.Status, rt.Type_name FROM Reimbursement r "
//					+ "JOIN Reimbursement_Status rs "
//					+ "ON r.Reimb_author = ? AND r.Reimb_status_id = rs.Status_id AND rs.Status_id = (SELECT status_id FROM Reimbursement_Status WHERE status = '?')"
//					+ "JOIN Reimbursement_Type rt"
//					+ "ON rt.Type_id = r.Reimb_type_id";
//			
//			PreparedStatement prepStat = con.prepareStatement(sql);
//			prepStat.setInt(1, Reimb_author);
//			prepStat.setString(2, status);
//			
//			ResultSet rs = prepStat.executeQuery();	
//			while(rs.next()) {
//				statusReimbursements.add(new CustomizeReimbursementOutput(rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getString(8), rs.getString(11), rs.getString(12)));
//			}
//		}catch(SQLException e) {
//			e.printStackTrace();
//		}
//		if(statusReimbursements.size()==0) {
//			return null;
//		}
//		return statusReimbursements;
//	}
	
//	public List<CustomizeReimbursementOutput> selectReimbursementByStatusResolver(int Reimb_resolver, String status) {
//		List<CustomizeReimbursementOutput> statusReimbursements = new ArrayList<>();
//		try(Connection con = dc.getConnection()){
//			String sql = "SELECT r.*, rs.Status, rt.Type_name FROM Reimbursement r "
//					+ "JOIN Reimbursement_Status rs "
//					+ "ON r.Reimb_resolver = ? AND r.Reimb_status_id = rs.Status_id AND rs.Status_id = (SELECT status_id FROM Reimbursement_Status WHERE status = '?')"
//					+ "JOIN Reimbursement_Type rt"
//					+ "ON rt.Type_id = r.Reimb_type_id";
//			
//			PreparedStatement prepStat = con.prepareStatement(sql);
//			prepStat.setInt(1, Reimb_resolver);
//			prepStat.setString(2, status);
//			
//			ResultSet rs = prepStat.executeQuery();	
//			while(rs.next()) {
//				statusReimbursements.add(new CustomizeReimbursementOutput(rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getString(7), rs.getString(11), rs.getString(12)));
//			}
//		}catch(SQLException e) {
//			e.printStackTrace();
//		}
//		if(statusReimbursements.size()==0) {
//			return null;
//		}
//		return statusReimbursements;
//	}
	
	public List<CustomizeReimbursementOutput> selectAllReimbursement() {
		List<CustomizeReimbursementOutput> statusReimbursements = new ArrayList<>();
		try(Connection con = dc.getConnection()){
			String sql = "SELECT u.First_name, u.Last_name, r.*, rs.Status, rt.Type_name FROM Reimbursement r "
					+ "JOIN Reimbursement_Status rs  "
					+ "ON r.Reimb_status_id = rs.Status_id  "
					+ "JOIN Reimbursement_Type rt "
					+ "ON r.Reimb_type_id = rt.Type_id "
					+ "JOIN Users u ON u.User_id = r.Reimb_author";		
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			
			ResultSet rs = prepStat.executeQuery();	
			while(rs.next()) {
				statusReimbursements.add(new CustomizeReimbursementOutput(rs.getString(1), rs.getString(2), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getBlob(8), rs.getString(13), rs.getString(14), rs.getInt(3)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		if(statusReimbursements.size()==0) {
			return null;
		}
		return statusReimbursements;
	}
	
	public List<CustomizeReimbursementOutput> selectReimbursementByStatusResolver(String status) {
		List<CustomizeReimbursementOutput> statusReimbursements = new ArrayList<>();
		try(Connection con = dc.getConnection()){
			String sql = "SELECT u.First_name, u.Last_name, r.*, rs.Status, rt.Type_name FROM Reimbursement r "
					+ "JOIN Reimbursement_Status rs "
					+ "ON r.Reimb_status_id = rs.Status_id AND r.Reimb_status_id = "
					+ "(SELECT Status_id FROM Reimbursement_Status WHERE Status = ?) "
					+ "JOIN Reimbursement_Type rt "
					+ "ON r.Reimb_type_id = rt.Type_id "
					+ "JOIN Users u ON u.User_id = r.Reimb_author";			
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setString(1, status);
			
			ResultSet rs = prepStat.executeQuery();	
			while(rs.next()) {
				statusReimbursements.add(new CustomizeReimbursementOutput(rs.getString(1), rs.getString(2), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getBlob(8), rs.getString(13), rs.getString(14), rs.getInt(3)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		if(statusReimbursements.size()==0) {
			return null;
		}
		return statusReimbursements;
	}
	
	public ReimbursementStatus getStatusByName(String Status) {
		ReimbursementStatus ReimbStatus = null;
		try(Connection con = dc.getConnection()){
			String sql = "SELECT * FROM Reimbursement_Status WHERE Status = ?";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setString(1, Status);
			
			ResultSet rs = prepStat.executeQuery();
			
			while(rs.next()) {
				ReimbStatus = new ReimbursementStatus(rs.getInt(1), rs.getString(2));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return ReimbStatus;
	}
	
	public ReimbursementType getTypeByName(String TypeName) {
		ReimbursementType ReimbStatus = null;
		try(Connection con = dc.getConnection()){
			String sql = "SELECT * FROM Reimbursement_Type WHERE Type_name = ?";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setString(1, TypeName);
			
			ResultSet rs = prepStat.executeQuery();
			
			while(rs.next()) {
				ReimbStatus = new ReimbursementType(rs.getInt(1), rs.getString(2));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return ReimbStatus;
	}
	
	public void Approved(int Reimb_id, int resolver, Timestamp resolved) {
		try(Connection con = dc.getConnection()){
			String sql = "UPDATE Reimbursement SET Reimb_status_id = (SELECT Status_id FROM Reimbursement_Status WHERE Status = 'Approved'), Reimb_resolver = ?, Reimb_resolved = ? "
					+ "WHERE Reimb_id = ? ";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setInt(1, resolver);
			prepStat.setTimestamp(2, resolved);
			prepStat.setInt(3, Reimb_id);
			
			prepStat.executeUpdate();					
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void Denied(int Reimb_id, int resolver, Timestamp resolved) {
		try(Connection con = dc.getConnection()){
			String sql = "UPDATE Reimbursement SET Reimb_status_id = (SELECT Status_id FROM Reimbursement_Status WHERE Status = 'Denied'), Reimb_resolver = ?, Reimb_resolved = ? "
					+ "WHERE Reimb_id = ? ";
			
			PreparedStatement prepStat = con.prepareStatement(sql);
			prepStat.setInt(1, resolver);
			prepStat.setTimestamp(2, resolved);
			prepStat.setInt(3, Reimb_id);
			
			prepStat.executeUpdate();			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

}
