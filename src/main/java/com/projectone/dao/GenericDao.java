package com.projectone.dao;

public interface GenericDao <E> {
	public E selectById(int id);
}
