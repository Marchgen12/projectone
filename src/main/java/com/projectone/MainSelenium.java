package com.projectone;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.projectone.page.SeleniumPage;

public class MainSelenium{

	public static void main(String[] args)  throws InterruptedException  {
		
		File file = new File("src/main/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		WebDriver driver = new ChromeDriver();
		
		SeleniumPage page = new SeleniumPage(driver);
		page.navigateTo();

	}

}
