package com.projectone.model;

public class Users {
	
	private int UserId;
	private String Username;
	private String Password;
	private String Firstname;
	private String Lastname;
	private String Email;
	private int UserRoleId;
	
	public Users() {
		// TODO Auto-generated constructor stub
	}
	
	public Users(String username, String password, String firstname, String lastname, String email, int userRoleId) {
		super();
		Username = username;
		Password = password;
		Firstname = firstname;
		Lastname = lastname;
		Email = email;
		UserRoleId = userRoleId;
	}


	public Users(int userId, String username, String password, String firstname, String lastname, String email,
			int userRoleId) {
		super();
		UserId = userId;
		Username = username;
		Password = password;
		Firstname = firstname;
		Lastname = lastname;
		Email = email;
		UserRoleId = userRoleId;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getFirstname() {
		return Firstname;
	}

	public void setFirstname(String firstname) {
		Firstname = firstname;
	}

	public String getLastname() {
		return Lastname;
	}

	public void setLastname(String lastname) {
		Lastname = lastname;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public int getUserRoleId() {
		return UserRoleId;
	}

	public void setUserRoleId(int userRoleId) {
		UserRoleId = userRoleId;
	}

	public int getUserId() {
		return UserId;
	}


	@Override
	public String toString() {
		return "Users [UserId=" + UserId + ", Username=" + Username + ", Password=" + Password + ", Firstname="
				+ Firstname + ", Lastname=" + Lastname + ", Email=" + Email + ", UserRoleId=" + UserRoleId
				+ "]";
	}	
}
