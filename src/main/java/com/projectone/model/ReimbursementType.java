package com.projectone.model;

public class ReimbursementType {

	private int TypeId;
	private String Type;
	
	public ReimbursementType() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementType(String type) {
		super();
		Type = type;
	}

	public ReimbursementType(int typeId, String type) {
		super();
		TypeId = typeId;
		Type = type;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public int getTypeId() {
		return TypeId;
	}

	@Override
	public String toString() {
		return "ReimbursementType [TypeId=" + TypeId + ", Type=" + Type + "]";
	}

	
}
