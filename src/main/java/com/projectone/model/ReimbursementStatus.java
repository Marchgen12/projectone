package com.projectone.model;

public class ReimbursementStatus {

	private int StatusId;
	private String Status;
	
	public ReimbursementStatus() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementStatus(String status) {
		super();
		Status = status;
	}

	public ReimbursementStatus(int statusId, String status) {
		super();
		StatusId = statusId;
		Status = status;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getStatusId() {
		return StatusId;
	}

	@Override
	public String toString() {
		return "ReimbursementStatus [StatusId=" + StatusId + ", Status=" + Status + "]";
	}

	
}
