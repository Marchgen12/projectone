package com.projectone.model;

public class UserRoles {

	private int UserRoleId;
	private String UserRole;
	
	public UserRoles() {
		// TODO Auto-generated constructor stub
	}
	
	public UserRoles(String userRole) {
		super();
		UserRole = userRole;
	}

	public UserRoles(int userRoleId, String userRole) {
		super();
		UserRoleId = userRoleId;
		UserRole = userRole;
	}

	public int getUserRoleId() {
		return UserRoleId;
	}

	public String getUserRole() {
		return UserRole;
	}

	public void setUserRole(String userRole) {
		UserRole = userRole;
	}


	@Override
	public String toString() {
		return "UserRoles [UserRoleId=" + UserRoleId + ", UserRole=" + UserRole + "]";
	}
	
	
}
