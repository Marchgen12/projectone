package com.projectone.model;

import java.sql.Blob;
import java.sql.Timestamp;

public class Reimbursement {

	private int ReimbId;	
	private int Amount;	
	private Timestamp Submitted;	
	private Timestamp Resolved;	
	private String Description;
	private Blob Reciept;	
	private int AuthorId;	
	private int ResolverId;	
	private int StatusFK;
	private int TypeFK;

	
	public Reimbursement() {
		// TODO Auto-generated constructor stub
	}


	public Reimbursement(int amount, Timestamp submitted, Timestamp resolved, String description, Blob reciept,
			int authorId, int resolverId, int statusFK, int typeFK) {
		super();
		Amount = amount;
		Submitted = submitted;
		Resolved = resolved;
		Description = description;
		Reciept = reciept;
		AuthorId = authorId;
		ResolverId = resolverId;
		StatusFK = statusFK;
		TypeFK = typeFK;
	}


	public Reimbursement(int reimbId, int amount, Timestamp submitted, Timestamp resolved, String description,
			Blob reciept, int authorId, int resolverId, int statusFK, int typeFK) {
		super();
		ReimbId = reimbId;
		Amount = amount;
		Submitted = submitted;
		Resolved = resolved;
		Description = description;
		Reciept = reciept;
		AuthorId = authorId;
		ResolverId = resolverId;
		StatusFK = statusFK;
		TypeFK = typeFK;
	}


	public int getAmount() {
		return Amount;
	}


	public void setAmount(int amount) {
		Amount = amount;
	}


	public Timestamp getSubmitted() {
		return Submitted;
	}


	public void setSubmitted(Timestamp submitted) {
		Submitted = submitted;
	}


	public Timestamp getResolved() {
		return Resolved;
	}


	public void setResolved(Timestamp resolved) {
		Resolved = resolved;
	}


	public String getDescription() {
		return Description;
	}


	public void setDescription(String description) {
		Description = description;
	}


	public Blob getReciept() {
		return Reciept;
	}


	public void setReciept(Blob reciept) {
		Reciept = reciept;
	}


	public int getAuthorId() {
		return AuthorId;
	}


	public void setAuthorId(int authorId) {
		AuthorId = authorId;
	}


	public int getResolverId() {
		return ResolverId;
	}


	public void setResolverId(int resolverId) {
		ResolverId = resolverId;
	}


	public int getStatusFK() {
		return StatusFK;
	}


	public void setStatusFK(int statusFK) {
		StatusFK = statusFK;
	}


	public int getTypeFK() {
		return TypeFK;
	}


	public void setTypeFK(int typeFK) {
		TypeFK = typeFK;
	}


	public int getReimbId() {
		return ReimbId;
	}


	@Override
	public String toString() {
		return "Reimbursement [ReimbId=" + ReimbId + ", Amount=" + Amount + ", Submitted=" + Submitted + ", Resolved="
				+ Resolved + ", Description=" + Description + ", Reciept=" + Reciept + ", AuthorId=" + AuthorId
				+ ", ResolverId=" + ResolverId + ", StatusFK=" + StatusFK + ", TypeFK=" + TypeFK + "]";
	}
	
		
}
