package com.projectone.model;

import java.sql.Blob;
import java.sql.Timestamp;

public class CustomizeReimbursementOutput {
	private String FirstName;
	private String LastName;
	private int amount;
	private Timestamp submitted;
	private Timestamp resolved;
	private String description;
	private int resolver;
	private Blob reciept;
	private String status;
	private String type;
	private int reimb_id;

	
	public CustomizeReimbursementOutput() {
		// TODO Auto-generated constructor stub
	}

	public CustomizeReimbursementOutput(int amount, Timestamp submitted, Timestamp resolved, String description, Blob reciept, int resolver, String status, String type) {
		super();
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.reciept = reciept;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}
	//rs.getString(1), rs.getString(2), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getBlob(8), rs.getString(13), rs.getString(14)
	
	public CustomizeReimbursementOutput(String firstName, String lastName, int amount, Timestamp submitted,
			Timestamp resolved, String description, Blob reciept, String status, String type, int reimb_id) {
		super();
		FirstName = firstName;
		LastName = lastName;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.reciept = reciept;
		this.status = status;
		this.type = type;
		this.reimb_id = reimb_id;
	}
	
	public CustomizeReimbursementOutput(String firstName, String lastName, int amount, Timestamp submitted,
			Timestamp resolved, String description, Blob reciept, int resolver, String status, String type) {
		super();
		FirstName = firstName;
		LastName = lastName;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.reciept = reciept;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}
	
	

	public CustomizeReimbursementOutput(String firstName, String lastName, int amount, Timestamp submitted,
			Timestamp resolved, String description, Blob reciept, int resolver, String status, String type,
			int reimb_id) {
		super();
		FirstName = firstName;
		LastName = lastName;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.resolver = resolver;
		this.reciept = reciept;
		this.status = status;
		this.type = type;
		this.reimb_id = reimb_id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Timestamp getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}

	public Timestamp getResolved() {
		return resolved;
	}

	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Blob getReciept() {
		return reciept;
	}

	public void setReciept(Blob reciept) {
		this.reciept = reciept;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public int getResolver() {
		return resolver;
	}

	public void setResolver(int resolver) {
		this.resolver = resolver;
	}

	public int getReimb_id() {
		return reimb_id;
	}

	public void setReimb_id(int reimb_id) {
		this.reimb_id = reimb_id;
	}

	
}
