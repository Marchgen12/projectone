package com.projectone.controller;

import org.eclipse.jetty.server.Authentication.User;

import com.projectone.model.Users;
import com.projectone.services.UserService;

import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public Handler postLogin = (ctx) ->{
		if(uServ.LoginCredentials(ctx.formParam("username"), ctx.formParam("password"))){
			if(uServ.getUser(ctx.formParam("username")).getUserRoleId() == uServ.getRole("Finance Ma").getUserRoleId()) {
				System.out.println("Login Credential Verified");
				ctx.sessionAttribute("LoginUser", uServ.getUser(ctx.formParam("username")));
				ctx.redirect("/html/financeManageViewAll.html");
			}else {
				System.out.println("Login Credential Verified");
				ctx.sessionAttribute("LoginUser", uServ.getUser(ctx.formParam("username")));
				ctx.redirect("/html/employeePage.html");
			}
		}else {
			System.out.println("Login not Verified");
			ctx.redirect("/html/tryAgainPage.html");
		}
	};
	
	
	//for logout set sessionAttribute LoginUser to null
	public Handler getSession =(ctx) -> {
		//System.out.println((Users)ctx.sessionAttribute("LoginUser"));
		Users user = (Users)ctx.sessionAttribute("LoginUser");
		ctx.json(user);
	};
	
}
