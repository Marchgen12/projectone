package com.projectone.controller;

import com.projectone.model.Users;
import com.projectone.services.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {

	private ReimbursementService rServ;
	
	public Handler createReimbursement = (ctx) -> {
		Users user = (Users)ctx.sessionAttribute("LoginUser");
		rServ.createReimbursement(Integer.parseInt(ctx.formParam("amount")), ctx.attribute("Stamp"), ctx.formParam("description"), null, user.getUserId(), "Pending", ctx.formParam("exampleRadios"));
		ctx.status(200);
		ctx.redirect("http://localhost:2345/html/reimbursementCreated.html");
	};
	
	public Handler viewAllReimbursements = (ctx) ->{
		if(rServ.Reimbursements().equals(null)) {
			ctx.status(404);
		}
		ctx.json(rServ.Reimbursements());
		ctx.status(200);
	};
	
	public Handler viewByApproved = (ctx) ->{
		if(rServ.ReimbursementByStatus("Approved").size()==0) {
			System.out.println("Handler!!!");
			ctx.status(404);
		}
		ctx.json(rServ.ReimbursementByStatus("Approved"));
	};
	
	public Handler viewByPending = (ctx) ->{
		if(rServ.ReimbursementByStatus("Pending").size()==0) {
			ctx.status(404);
		}
		ctx.json(rServ.ReimbursementByStatus("Pending"));
	};
	
	public Handler viewByDenied = (ctx) ->{
		if(rServ.ReimbursementByStatus("Denied").size()==0) {
			ctx.status(404);
		}
		ctx.json(rServ.ReimbursementByStatus("Denied"));
	};
	
	public Handler userReimbursements = (ctx)->{
		Users user = (Users)ctx.sessionAttribute("LoginUser");
		ctx.json(rServ.ReimbursementByUser(user.getUserId()));
	};
	
	public Handler approveReimbursement = (ctx) -> {
		Users user = (Users)ctx.sessionAttribute("LoginUser");
		rServ.changeToApproved(Integer.parseInt(ctx.pathParam("id")), user.getUserId(), ctx.attribute("Stamp"));
	};
	
	public Handler deniedReimbursement = (ctx) -> {
		Users user = (Users)ctx.sessionAttribute("LoginUser");
		rServ.changeToDenied(Integer.parseInt(ctx.pathParam("id")), user.getUserId(), ctx.attribute("Stamp"));
	};
	
	public ReimbursementController() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}
	
	
}