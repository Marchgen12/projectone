package com.projectone.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SeleniumPage {

	private WebDriver driver;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement submitButton;
	
	public SeleniumPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.usernameField = driver.findElement(By.name("username"));
		this.passwordField = driver.findElement(By.name("password"));
		this.submitButton = driver.findElement(By.name("submitLogin"));
	}

	public String getUsernameField() {
		return this.usernameField.getAttribute("value");
	}

	public void setUsernameField(String name) {
		this.usernameField.clear();
		this.usernameField.sendKeys(name);
	}

	public String getPasswordField() {
		return this.passwordField.getAttribute("value");
	}

	public void setPasswordField(String password) {
		this.passwordField.clear();
		this.passwordField.sendKeys(password);
	}

	public void SubmitButton() {
		this.submitButton.click();
	}

	public void navigateTo(){
		this.driver.get("http://localhost:2345/html/homePageLogin.html");
	}

	
}
