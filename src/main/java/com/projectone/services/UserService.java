package com.projectone.services;

import com.projectone.dao.UserDao;
import com.projectone.model.UserRoles;
import com.projectone.model.Users;

public class UserService {

	private UserDao uDao;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}

	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}

	public Users getUser(String username) {
		Users user = uDao.selectByUsername(username);
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public boolean LoginCredentials(String username, String password) {
		Users user = uDao.selectByUsername(username);
		if(user.getPassword().equals(password)){
			return true;
		}return false;
	}
	
	public UserRoles getRole(String role) {
		UserRoles uRole = uDao.selectRole(role);
		if(uRole == null) {
			throw new NullPointerException();
		}
		return uRole;
	}
	
}
