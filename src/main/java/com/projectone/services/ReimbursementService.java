package com.projectone.services;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.List;

import com.projectone.dao.ReimbursementDao;
import com.projectone.model.CustomizeReimbursementOutput;
import com.projectone.model.Reimbursement;
import com.projectone.model.ReimbursementStatus;
import com.projectone.model.ReimbursementType;

public class ReimbursementService {
	private ReimbursementDao rDao;
	
	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}
	
	public void createReimbursement(int amount, Timestamp submitted, String description, Blob receipt, int author, String status, String type) {
		ReimbursementStatus rs = rDao.getStatusByName(status);
		ReimbursementType rt = rDao.getTypeByName(type);
		
		rDao.createReimbursement(amount, submitted, description, receipt, author, rs.getStatusId(), rt.getTypeId());		
	}
	
	public List<CustomizeReimbursementOutput> ReimbursementByUser(int author_id){
		if(rDao.selectByAuthorId(author_id).size() == 0) {
			return null;
		}
		return rDao.selectByAuthorId(author_id);
	}
	
	public List<CustomizeReimbursementOutput> ReimbursementByStatus(String Status){
		if(rDao.selectReimbursementByStatusResolver(Status).size() == 0) {
			//System.out.println("not reimbursement with this status");
			return null;
		}
		//System.out.println(rDao.selectReimbursementByStatusResolver(Status));
		return rDao.selectReimbursementByStatusResolver(Status);
	}
	
	public List<CustomizeReimbursementOutput> Reimbursements(){
		if(rDao.selectAllReimbursement().size() == 0) {
			return null;
		}
		//System.out.println(rDao.selectAllReimbursement());
		return rDao.selectAllReimbursement();
	}
	
	public void changeToApproved(int Reimb_id, int resolver, Timestamp resolved) {
		rDao.Approved(Reimb_id, resolver, resolved);
	}
	
	public void changeToDenied(int Reimb_id, int resolver, Timestamp resolved) {
		rDao.Denied(Reimb_id, resolver, resolved);
	}
}
