
window.onload = function(){
	getSession()
	getJSON();
	
	var Stamp = new Date();
    var date = Stamp.getFullYear()+'-'+(Stamp.getMonth()+1)+'-'+Stamp.getDate()+' '+Stamp.getHours()+':'+Stamp.getMinutes()+':'+Stamp.getSeconds();
    console.log(date);
}

function getSession(){
	let xhttp = new XMLHttpRequest;
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState == 4 && xhttp.status == 200){
			let sessionUser = JSON.parse(xhttp.responseText);
/*			console.log(sessionUser);*/
		}
	}
	
	xhttp.open("GET", "http://localhost:2345/Login/Session");
	
	xhttp.send();
}

function getJSON(){
	let xhttp = new XMLHttpRequest;
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState == 4 && xhttp.status == 200){
			let Reimbursements = JSON.parse(xhttp.responseText);
			//console.log(Reimbursements);
			TableManip(Reimbursements);
		}
	}	
	
	xhttp.open("GET", "http://localhost:2345/User/Reimbursements");
	
	xhttp.send();
}

function TableManip(ReimbursementsJSON){
	let table = document.getElementById('tableBody');
	for(var i = 0; i < ReimbursementsJSON.length; i++){
		table.innerHTML += 	`<tr><td>$${ReimbursementsJSON[i].amount}</td>` +
							`<td>${ReimbursementsJSON[i].description}</td>`+
							`<td>${ReimbursementsJSON[i].status}</td>`+
							`<td>` + dateConvert(ReimbursementsJSON[i].submitted) + `</td>`+
							`<td>` + dateConvert(ReimbursementsJSON[i].resolved) + `</td>`+
							`<td>${ReimbursementsJSON[i].type}</td>`+
							`<td>${ReimbursementsJSON[i].resolver}</td></tr>`;
							
	}
	document.getElementById('Greeter').innerText = `Hello ${ReimbursementsJSON[0].firstName} ${ReimbursementsJSON[0].lastName}`;
};

function dateConvert(number){
	if(number == null){
		return "";
	}
	let day = new Date(number);
	stringDate = day.getFullYear()+"-"+(day.getMonth()+1)+"-"+day.getDate()+" "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds();
	return stringDate;	
}
