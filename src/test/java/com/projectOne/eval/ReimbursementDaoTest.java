package com.projectOne.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.projectone.dao.DatabaseConnection;
import com.projectone.dao.ReimbursementDao;
import com.projectone.model.ReimbursementStatus;
import com.projectone.model.ReimbursementType;

@RunWith(MockitoJUnitRunner.class)
public class ReimbursementDaoTest {

	private String url = "jdbc:mariadb://database-1.cg7cn5qcgpss.us-east-2.rds.amazonaws.com:3306/project1ver2db";
	private String username = "project1ver2user";
	private String password = "mypassword";
	
	@Mock
	private DatabaseConnection dc = new DatabaseConnection();
	
	@Mock
	private Connection mcon;
	
	@Mock
	private PreparedStatement prepStat;
	
	@Mock
	private ResultSet rs;
	
//	private Reimbursement testReimbursement;
	private ReimbursementStatus testReimbursementStatus;
	private ReimbursementType testReimbursementType;
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	///NOTE I cant do testing in Reimbursement because the timestamp format is not letting me mock it
	///specially time_submitted cannot be set to null so it will cause a null pointer exception if I mock
	///both timestamps with null;
//	@Before
//	public void setUpReimbursement() throws Exception {
//		MockitoAnnotations.initMocks(this);
//		when(dc.getConnection()).thenReturn(mcon);
//		when(mcon.prepareStatement(any(String.class))).thenReturn(prepStat);
//		testReimbursement = new Reimbursement(5018, 65, '2021-01-11 06:57:14.000', '2021-01-11 06:57:14.000', "Uber ride", null, 2000, 2001, 1002, 1004 );
//		when(rs.first()).thenReturn(true);
//		when(prepStat.executeQuery()).thenReturn(rs);
//	}
	
	@Before
	public void setUpReimbursementStatus() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dc.getConnection()).thenReturn(mcon);
		when(mcon.prepareStatement(any(String.class))).thenReturn(prepStat);
		testReimbursementStatus = new ReimbursementStatus(1000, "Pending");
		when(prepStat.executeQuery()).thenReturn(rs);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testReimbursementStatus.getStatusId());
		when(rs.getString(2)).thenReturn(testReimbursementStatus.getStatus());
		
	}
	
//	@Before
//	public void setUpReimbursementType() throws Exception {
//		MockitoAnnotations.initMocks(this);
//		when(dc.getConnection()).thenReturn(mcon);
//		when(mcon.prepareStatement(any(String.class))).thenReturn(prepStat);
//		testReimbursementType = new ReimbursementType(1005, "Food");
//	    when(prepStat.executeQuery()).thenReturn(rs);
//		when(rs.first()).thenReturn(true);
//		when(rs.getInt(1)).thenReturn(testReimbursementType.getTypeId());
//		when(rs.getString(2)).thenReturn(testReimbursementType.getType());
//	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void checkByStatusName() {
//		ReimbursementDao reimb = new ReimbursementDao(dc);
//		System.out.println(reimb.getStatusByName("Pending"));
		assertEquals(new ReimbursementDao(dc).getStatusByName("Pending").getStatusId(), testReimbursementStatus.getStatusId());
	}
	
	@Test
	public void checkByTypeName() {
		assertEquals(new ReimbursementDao(dc).getTypeByName("Food").getTypeId(), testReimbursementType.getTypeId());
	}


}

