package com.projectOne.eval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.projectone.page.SeleniumPage;

public class SeleniumPageTest {

	private SeleniumPage page;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		String path ="src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", path);
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception{
		this.page = new SeleniumPage(driver);
	}
	
	@After
	public void tearDown() throws Exception{}
	
	@Test
	public void testSuccessLogin() {
		page.setUsernameField("Sample");
		page.setPasswordField("1234");
		page.SubmitButton();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/html/employeePage.html"));
		assertEquals("http://localhost:2345/html/homePageLogin.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testFailedLogin() {
		page.setUsernameField("Sample");
		page.setPasswordField("Sample");
		page.SubmitButton();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/html/tryAgainPage.html"));
		assertEquals("http://localhost:2345/html/tryAgainPage.html", driver.getCurrentUrl());
	}
}
