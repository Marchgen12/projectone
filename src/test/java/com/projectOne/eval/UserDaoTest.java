package com.projectOne.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.projectone.dao.DatabaseConnection;
import com.projectone.dao.UserDao;
import com.projectone.model.UserRoles;
import com.projectone.model.Users;


public class UserDaoTest {

	private String url = "jdbc:mariadb://database-1.cg7cn5qcgpss.us-east-2.rds.amazonaws.com:3306/project1ver2db";
	private String username = "project1ver2user";
	private String password = "mypassword";
	
	@Mock
	private DatabaseConnection dc;
	
	@Mock
	private Connection mcon;
	
	@Mock
	private PreparedStatement prepStat;
	
	@Mock
	private ResultSet rs;
	
	private Users testUser;
	private UserRoles testUserRole;

	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUpUser() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dc.getConnection()).thenReturn(mcon);
		when(mcon.prepareStatement(any(String.class))).thenReturn(prepStat);
		testUser = new Users(2000, "ProjectOne", "1234", "Revature", "Pro", "p1@yahoo.com", 1007);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getUserId());
		when(rs.getString(2)).thenReturn(testUser.getUsername());
		when(rs.getString(3)).thenReturn(testUser.getPassword());
		when(rs.getString(4)).thenReturn(testUser.getFirstname());
		when(rs.getString(5)).thenReturn(testUser.getLastname());
		when(rs.getString(6)).thenReturn(testUser.getEmail());
		when(rs.getInt(7)).thenReturn(testUser.getUserRoleId());
		when(prepStat.executeQuery()).thenReturn(rs);
	}
	
	@Before
	public void setUpUserRole() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dc.getConnection()).thenReturn(mcon);
		when(mcon.prepareStatement(any(String.class))).thenReturn(prepStat);
		testUserRole = new UserRoles(1008, "Finance Ma");
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUserRole.getUserRoleId());
		when(rs.getString(2)).thenReturn(testUserRole.getUserRole());
		when(prepStat.executeQuery()).thenReturn(rs);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByUsername() {
		assertEquals(new UserDao(dc).selectByUsername("ProjectOne").getUserId(), testUser.getUserId());
	}

	@Test
	public void testFindByEmail() {
		assertEquals(new UserDao(dc).selectByEmail("p1@yahoo.com").getPassword(), testUser.getPassword());
	}
	
	@Test
	public void testFindByRoleName() {
		assertEquals(new UserDao(dc).selectRole("Finance Ma").getUserRoleId(), testUserRole.getUserRoleId());
	}
}
