# Employee Reimbursement System

Employee Reimbursement System is a full stack application that handled approval and denial of pending reimbursements that is created by a employee of the company. There are two employee roles - finance manager and employee - that interacts with the following application.

## Technologies that are utilized in the creation of the project are:
    * Java
    * HTML
    * CSS
    * JavaScript
    * BootStrap
    * Javalin
    * JUnit
    * Mockito
    * Selenium
    * Hibernate(this is used in the first iteration of the project to create the table and will be further improved upon in the later version of 
        the  project)

## The main features of this application are:
    Employees will be able to log in with their account and create a reimbursement request to the financial manager, this will have the initial
        status of pending until approve or denied by a financial manager.
    Financial managers are the one to decide if a certain reimbursement is with accordance to the policy of the company and will have access on 
        denying, approving, creating and viewing said reimbursements.

# Getting started
git clone https://gitlab.com/Marchgen12/projectone.git
then create a local branch git checkout -b "local branch name"
then merge the master branch to local branch git merge master
then you will be able to have access to the codes that are available in this repository

# Usage
Login with an employee account to create reimbursement(s)
Username: Sample
Password: 1234
this will be an example of an employee login credentials

Login with a financial manager account to create, approve, deny or view past and current reimbursement(s)
Username: Boss
Password: 1234
this in an example of a financial manager login credentials

Contributor
Jugen Fornoles



