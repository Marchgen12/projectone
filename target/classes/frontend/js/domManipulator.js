/**
 * 
 */
window.onload = function(){
	getSession();
	
	var Stamp = new Date();
    var date = Stamp.getFullYear()+'-'+(Stamp.getMonth()+1)+'-'+Stamp.getDate()+' '+Stamp.getHours()+':'+Stamp.getMinutes()+':'+Stamp.getSeconds();
    console.log(date);
}

function getSession(){
	let xhttp = new XMLHttpRequest;
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState == 4 && xhttp.status == 200){
			let Reimbursements = JSON.parse(xhttp.responseText);
			//console.log(Reimbursements);
			TableManip(Reimbursements);
		}
	}
	
	xhttp.open("GET", "/FinanceManager/Reimbursements");
	
	xhttp.send();
}

function TableManip(ReimbursementsJSON){
	let table = document.getElementById('tableBody');
	for(var i = 0; i < ReimbursementsJSON.length; i++){
		table.innerHTML += 	`<tr><td>${ReimbursementsJSON[i].firstName}</td>` + 
							`<td>${ReimbursementsJSON[i].lastName}</td>` +
							`<td>$${ReimbursementsJSON[i].amount}</td>` +
							`<td>${ReimbursementsJSON[i].description}</td>`+
							`<td>${ReimbursementsJSON[i].status}</td>`+
							`<td>${ReimbursementsJSON[i].type}</td></tr>`;
	}
	document.getElementById('Greeter').innerText = `Hello ${ReimbursementsJSON[0].firstName} ${ReimbursementsJSON[0].lastName}`;
}
